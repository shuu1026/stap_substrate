
#define FCY 10000000
#include <stdio.h>
#include <stdlib.h>
#include <xc.h>
#include <stdint.h>
#include <stdbool.h>
#include <libpic30.h>
#include "RS422M.h"

uint16_t number_of_valid_data = 0; //有効データ数 ※データが超膨大でないことを仮定(最大:short:32台,64個の32byteの配列)
uint8_t buffer_422M[BUFFER_SIZE] = {};
uint16_t now_rs422_position = 0;
uint8_t now_buffer_position = 0;
rs422_data_position index_valid[(MAX_DEVICE_KINDS)*(MAX_DATA_KINDS)] = {};
rs422msg device_id[MAX_DEVICE_KINDS] = {};
//device_id[個体識別番号].data_id[データ群識別番号(別紙説明書参照)].data[データ番号]

/*遠隔書き込み用*/
rs422_data_EEPROM* datas_write;
rs422_data* datas_read;
uint8_t status_I2C = 0xFF;
uint8_t adress_device, adress_data;
rs422_data_EEPROM buffer_EEPROM[2] = {};

void Start_RS422(void) {

    uint8_t number_of_invalid_data = 0; //無効データ数(単位デバイス当たりの無効データ数であり、途中計算用)
    uint8_t search_deviceID_local = 0, search_dataID_local = 0;
    number_of_valid_data = 0;

    while (1) {

        if ((device_id[search_deviceID_local].data_id[search_dataID_local].status.type) != invalid_DATAtype) {

            if ((device_id[search_deviceID_local].data_id[search_dataID_local].status.type) == (transmit_DATAtype)) {
                adress_device = search_deviceID_local; //デバイスid
                adress_data = search_dataID_local; //データid
                datas_read = &(device_id[search_deviceID_local].data_id[search_dataID_local]);

                while (readEEPROM_RS422M() == false);
                while (status_I2C != 0xFF);


            }
            LATAbits.LATA0 = 1;
            index_valid[number_of_valid_data].number_device_ID = search_deviceID_local;
            index_valid[number_of_valid_data].number_data_ID = search_dataID_local;
            number_of_valid_data++;
        }//有効なデータであれば、number_of_valid_dataを増加

        else {
            number_of_invalid_data++;
        }//無効なデータであれば、number_of_invalid_dataを増加

        /*if (number_of_invalid_data == MAX_DATA_KINDS) {
            break;
        }//無効データ数がデバイス当たりMAX_DATA_KINDSと等しければブレイクする。(デバイスIDは連番であることを仮定)※修正か？*/

        if ((search_deviceID_local >= ((MAX_DEVICE_KINDS) - 1)) && (search_dataID_local >= ((MAX_DATA_KINDS) - 1))) {
            break;
        }//search_deviceID_localがMAX_DEVICE_KINDSと等しく、search_dataID_localがMAX_DATA_KINDSを超えればブレイクする ※（配列の添え字==要素数-1）

        else if (search_dataID_local >= (MAX_DATA_KINDS - 1)) {
            search_dataID_local = 0;
            search_deviceID_local++;
            number_of_invalid_data = 0;
        }//search_dataID_localがMAX_DATA_KINDSを超えたとき,serch_dataID=0,search_deviceID_local++;

        else {
            search_dataID_local++;
        }//search_dataID_localを増加

    }
    search_deviceID_local = 0;
    search_dataID_local = 0;

    if (number_of_valid_data >= 1) {
        now_rs422_position = 0;
        set_next_mes(); //beffer[0]を送信開始しています
    }//１つ以上の有効データ

    return;

}

void set_next_mes(void) {
    //バッファ内のmesを送信し終わったら呼び出す。
    uint8_t i = 0;
    static uint8_t first_data_send = 0;


    if (first_data_send == 0) {
        first_data_send = 0x1; //0bit目をON
    } else {
        now_rs422_position++;
        if (now_rs422_position > ((number_of_valid_data) - 1)) {
            now_rs422_position = 0;
        }
    }

    buffer_422M[0] = ((&device_id[search_deviceID])-(device_id)); //device_id
    buffer_422M[1] = ((&device_id[search_deviceID].data_id[search_dataID])-(device_id[search_deviceID].data_id)); //data_id
    buffer_422M[2] = (device_id[search_deviceID].data_id[search_dataID].length); //length

    if ((device_id[search_deviceID].data_id[search_dataID].status.type) == (transmit_DATAtype)) {
        for (i = 0; i < device_id[search_deviceID].data_id[search_dataID].length; i++) {
            buffer_422M[(i + 3)] = device_id[search_deviceID].data_id[search_dataID].data[i]; //長さ分のみバッファに格納
        }//bufferの位置に注意
    }

    device_id[search_deviceID].data_id[search_dataID].status.now_RS422 = now_transmit;
    now_buffer_position = 1;
    RS422_TXIF = 0;
    putUART_RS422(buffer_422M[0], UART_add_type);
    RS422_TXIE = 1;

    return;

}

void rx_ISR_RS422() {

    //UARTエラー処理(未実装)
    RS422_RXIF = 0;
    static uint16_t now_recieve_data = 0;
    static uint8_t recieve_times_rs422 = 0;

    now_recieve_data = RS422_RXREG;
    now_recieve_data = (now_recieve_data & 0x00FF);


    if (device_id[search_deviceID].data_id[search_dataID].status.now_RS422 == now_recieve) {
        if ((device_id[search_deviceID].data_id[search_dataID].status.type) == (transmit_DATAtype)) {

            if (now_recieve_data == 6) {
                device_id[search_deviceID].data_id[search_dataID].status.CHECK = SUCCESSFUL_RS422;
                tmr_setup_RS422(); //送信(M->S)の成功を確認、次のデータを送る
            } else {
                //set_now_mes_again();
            }//再送

            recieve_times_rs422 = 0;

        }//送信データの時の動作
            /***********************************************************/
        else if ((device_id[search_deviceID].data_id[search_dataID].status.type) == (recieve_DATAtype)) {

            recieve_times_rs422++;
            if (recieve_times_rs422 == 1) {

                if (now_recieve_data != ((&device_id[search_deviceID])-(device_id))) {
                    //device_id[search_deviceID].data_id[search_dataID].status.CHECK = FAILURE_RS422; //通信失敗
                    //set_now_mes_again(); //もう一度送信を開始
                }//相手はデバイスIDを送ってくる

            }//１回目つまり、相手が自身のIDを送ってきたとき
            else {
                device_id[search_deviceID].data_id[search_dataID].data[((recieve_times_rs422) - 2)] = now_recieve_data;

                if ((recieve_times_rs422 - 1) == (device_id[search_deviceID].data_id[search_dataID].length)) {
                    set_ACK_RS422(); //ACKの返信
                    recieve_times_rs422 = 0;
                }//正常に通信を終える
            }//受信データをデータベースに格納

        }//受信データ(S->M)を受け取るとき
    }


    return;
}

void tx_ISR_RS422() {
    RS422_TXIF = 0;

    static uint8_t tx_ISR_times = 0;

    if (device_id[search_deviceID].data_id[search_dataID].status.now_RS422 == now_reply) {
        RS422_TXIE = 0;
        device_id[search_deviceID].data_id[search_dataID].status.CHECK = SUCCESSFUL_RS422;
        tmr_setup_RS422(); //96bitの待ち後に再開
        return;
    }

    if (tx_ISR_times == 0) {
        tx_ISR_times++;
        Start_RS422();
        return;
    }
    if (now_buffer_position == 0) {
        //未実装
        while (1);
    }//0番目は必ずset_next_mes()関数にて行われる

    putUART_RS422((buffer_422M[now_buffer_position]), UART_data_type); //次のデータを送信準備

    if (((now_buffer_position == (2 + (device_id[search_deviceID].data_id[search_dataID].length))) && (device_id[search_deviceID].data_id[search_dataID].status.type == (transmit_DATAtype)))
            || ((now_buffer_position == 2) && (device_id[search_deviceID].data_id[search_dataID].status.type == (recieve_DATAtype)))) {
        //送信データで、length分の送信が終わったとき
        //受信データで、deviceID,dataID,lengthの送信が終わったとき

        RS422_TXIE = 0; //割り込みを停止
        device_id[search_deviceID].data_id[search_dataID].status.now_RS422 = now_recieve;
        return;
    }

    now_buffer_position++;

    return;
}

/******************************************************************************************/
/*ここから遠隔書き込み関数*/

/******************************************************************************************/



bool writeEEPROM_RS422M(void) {
    static uint8_t times_call_write = 1;
    if ((status_I2C != 0xFF) && (times_call_write == 1)) {
        return false;
    }

    status_I2C = 0; //EEPROM書き込み

    if (times_call_write == 0) {

        times_call_write = 1;
        putUART2(6); //"ACK"の返信
        while (toEEPROM_TXIF == 0);
        toEEPROM_TXIF = 0;
        status_I2C = 0xFF;
        return true;

    }//正しい初回呼び出しへの準備

    else if (times_call_write == 1) {
        I2C1CONbits.SEN = 1;
    }//スタート条件生成

    else if (times_call_write == 2) {

        I2C1TRN = (((0x50 << 1)&0xFE) | 0);

    }//チップ選択送信

    else if (times_call_write == 3) {

        I2C1TRN = adress_device;

    }//アドレスH

    else if (times_call_write == 4) {

        I2C1TRN = (((adress_data & 0x0F) << 4)&0xF0);

    }//アドレスL

    else if (times_call_write <= (4 + buffer_EEPROM[0].length)) {

        I2C1TRN = datas_write->data[(times_call_write - 5)];

    }//ページ書き込み
    else {
        I2C1CONbits.PEN = 1;
        times_call_write = 0;
        return true;
    }//ストップ条件生成

    times_call_write++;
    return true;
}

bool readEEPROM_RS422M(void) {
    static uint8_t times_call_read = 1;
    if ((status_I2C != 0xFF) && times_call_read == 1) {
        return false;
    }
    status_I2C = 1; //EEPROM読み込み

    if (times_call_read == 0) {

        times_call_read = 1;
        status_I2C = 0xFF;

        return true;

    }//正しい初回呼び出しへの準備

    else if (times_call_read == 1) {

        I2C1CONbits.SEN = 1;

    }//スタート条件生成

    else if (times_call_read == 2) {

        I2C1TRN = (((0x50 << 1)&0xFE) | 0);
    }//チップ選択送信

    else if (times_call_read == 3) {
        I2C1TRN = adress_device;
    }//アドレスH

    else if (times_call_read == 4) {
        I2C1TRN = ((adress_data << 4)&0xF0);
    }//アドレスL

    else if (times_call_read == 5) {
        I2C1CONbits.RSEN = 1;
    }//リピートスタート条件生成

    else if (times_call_read == 6) {
        I2C1TRN = (((0x50 << 1)&0xFE) | 1);
    }//相手アドレスの送信

    else if (times_call_read == 7) {
        I2C1CONbits.RCEN = 1;
    }//受信有効化

    else {

        if (times_call_read == (MAX_DATA_SIZE * 2) + 8) {

            I2C1CONbits.ACKDT = 1;
            I2C1CONbits.ACKEN = 1;

        } else if (times_call_read == (MAX_DATA_SIZE * 2) + 9) {
            I2C1CONbits.PEN = 1;
            times_call_read = 0;
            return true;
        } else if ((times_call_read % 2) == 0) {
            datas_read->data[((times_call_read - 8) / 2)] = I2C1RCV;
            I2C1CONbits.ACKDT = 0;
            I2C1CONbits.ACKEN = 1;
        } else {
            I2C1CONbits.RCEN = 1;
        }
    }

    times_call_read++;
    return true;
}

void rx_ISR_toEEPROM(void) {
    static uint8_t recieve_times_rs422 = 0; //受信回数カウント
    uint16_t now_recieve_data_toEEPROM = 0;
    now_recieve_data_toEEPROM = (toEEPROM_RXREG & 0xFF);

    uint8_t times1, times2;

    if (recieve_times_rs422 == 0) {
        buffer_EEPROM[0].device_id = now_recieve_data_toEEPROM;
    }//1回目の受信である

    else if (recieve_times_rs422 == 1) {
        buffer_EEPROM[0].data_id = now_recieve_data_toEEPROM;
    }//2回目の受信である

    else if ((recieve_times_rs422) == 2) {
        buffer_EEPROM[0].length = now_recieve_data_toEEPROM;
        if (now_recieve_data_toEEPROM == 0x99) {
            recieve_times_rs422 = 0;
            toEEPROM_RXIF = 0; //受信割り込みフラグの初期化
            for (times1 = 0; times1 < 16; times1++) {
                buffer_EEPROM[0].data[times1] = 0;
            }
            for (times1 = 0; times1 < 16; times1++) {
                for (times2 = 0; times2 < 16; times2++) {
                    adress_device = times1; //デバイスid
                    adress_data = times2; //データid
                    datas_write = &(buffer_EEPROM[0]);
                    while (writeEEPROM_RS422M() != true);
                    while (status_I2C != 0xFF);
                }
            }
            return;
        }
    }//3回目の受信である

    else {

        buffer_EEPROM[0].data[(recieve_times_rs422 - 3)] = (now_recieve_data_toEEPROM & 0xFF); //受信データを格納する


        if (recieve_times_rs422 >= (buffer_EEPROM[0].length + 2)) {
            recieve_times_rs422 = 0;
            toEEPROM_RXIF = 0; //受信割り込みフラグの初期化

            /*EEPROM書き込み*/
            adress_device = buffer_EEPROM[0].device_id; //デバイスid
            adress_data = buffer_EEPROM[0].data_id; //データid
            datas_write = &(buffer_EEPROM[0]);
            while (writeEEPROM_RS422M() != true);
            while (status_I2C != 0xFF);
            return;
        }//length個分の受信をしたら"ACK"の返信

    }//4回目以降の受信である

    toEEPROM_RXIF = 0;
    recieve_times_rs422++;
    return;
}
#define not_go_area (20.0)

void mecanum_Control(bool GOorTURN, uint8_t x_mecanum, uint8_t y_mecanum) {

    /*  注意    */
    //GOorTURN  :旋回か移動か 
    //x_mecanum :アナログスティックのx方向データ
    //y_mecanum :アナログスティックのy方向データ
    //unsigned型で引数に入れてください

    double x_calculation = 0.0, y_calculation = 0.0; //符号付きの処理

    x_calculation = ((double) ((int8_t) x_mecanum - 128));
    y_calculation = ((double) ((int8_t) y_mecanum - 128));

    double duty_FR_percent = 0.0, duty_FL_percent = 0.0;
    double duty_BR_percent = 0.0, duty_BL_percent = 0.0;
    /****************************/
    /*  重要                    */
    //F:flont(前) B:back(後)
    //R:right(右) L:left(左)
    //例:FR(前右)
    /******************************/
    double middle_calculation = 0.0;
    /*  以下、計算*/

    /*  十字型　不感領域*/
    if (fabs(x_calculation) < not_go_area) {
        x_calculation = not_go_area;
    }
    if (fabs(y_calculation) < not_go_area) {
        y_calculation = not_go_area;
    }
    //not_go_area:アナログスティックの不感領域のこと

    /******************     上限設定１        ******************************/
    /*************/
    /* x値の調整 */
    /*************/
    if (x_calculation > 0.0) {
        x_calculation = (x_calculation - not_go_area) * 2.0; //不感領域分を引く　*2.0は上がる傾き
        if (x_calculation >= 100.0) {
            x_calculation = 100.0;
        }//percentの上限設定
    }//正のとき
    else {
        x_calculation = (x_calculation + not_go_area) * 2.0; //不感領域分を引く　*2.0は上がる傾き
        if (x_calculation <= -100.0) {
            x_calculation = -100.0;
        }//percentの上限設定
    }//負のとき
    /************/
    /* y値の調整 */
    /************/
    if (y_calculation > 0.0) {
        y_calculation = (y_calculation - not_go_area) * 2.0; //不感領域分を引く　*2.0は上がる傾き
        if (y_calculation >= 100.0) {
            y_calculation = 100.0;
        }//percentの上限設定
    }//正のとき
    else {
        y_calculation = (y_calculation + not_go_area) * 2.0; //不感領域分を引く　*2.0は上がる傾き
        if (y_calculation <= -100.0) {
            y_calculation = -100.0;
        }//percentの上限設定
    }//負のとき
    /******************     途中計算　終了   ******************************/
    if (GOorTURN == GO_mode_mecanum) {

        duty_FR_percent = ((-1.0) * x_calculation) + y_calculation;
        duty_FL_percent = x_calculation + y_calculation;
        duty_BR_percent = duty_FL_percent; //BRはFLと同じ動き
        duty_BL_percent = duty_FR_percent; //BLはFRと同じ動き

    }//x方向とy方向の重ね合わせをしている

    else {
        if (x_mecanum > 0) {

            middle_calculation = (x_calculation - not_go_area);
            duty_FR_percent = middle_calculation;
            duty_FL_percent = (-1) * duty_FR_percent;
            duty_BR_percent = duty_FR_percent;
            duty_BL_percent = duty_FL_percent;

        } else {

            middle_calculation = (x_calculation + not_go_area);
            duty_FR_percent = middle_calculation;
            duty_FL_percent = (-1) * duty_FR_percent;
            duty_BR_percent = duty_FR_percent;
            duty_BL_percent = duty_FL_percent;

        }

    }//旋回

    /*      duty比の上限の設定      */
    /*      100% ~ -100% のduty設定 */
    if (duty_FR_percent > 100.0) {
        duty_FR_percent = 100.0;
    } else if (duty_FR_percent < -100.0) {
        duty_FR_percent = -100.0;
    }
    if (duty_FL_percent > 100.0) {
        duty_FL_percent = 100.0;
    } else if (duty_FL_percent < -100.0) {
        duty_FL_percent = -100.0;
    }
    if (duty_BR_percent > 100.0) {
        duty_BR_percent = 100.0;
    } else if (duty_BR_percent < -100.0) {
        duty_BR_percent = -100.0;
    }
    if (duty_BL_percent > 100.0) {
        duty_BL_percent = 100.0;
    } else if (duty_BL_percent < -100.0) {
        duty_BL_percent = -100.0;
    }
    /***************************************************/
    /*      duty比の設定        */
    dutyA_percent_RS422M(0) = (duty_FR_percent * 0.01);
    dutyB_percent_RS422M(0) = (duty_FL_percent * 0.01);
    dutyA_percent_RS422M(1) = (duty_BR_percent * 0.01);
    dutyB_percent_RS422M(1) = (duty_BL_percent * 0.01);

    return;
}



