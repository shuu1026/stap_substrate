/* 
 * File:   defines_for_RS422.h
 * Author: Shuuya
 *
 * Created on 2016/07/11, 18:39
 */

#ifndef DEFINES_FOR_RS422_H
#define	DEFINES_FOR_RS422_H

#ifdef	__cplusplus
extern "C" {
#endif
 
#ifndef MOTER_CONTROLLER
#define MOTER_CONTROLLER (0)  //データ個数7個
#endif
    
#ifndef SERVO_CONTROLLER
#define SERVO_CONTROLLER (7)  //データ個数1個
#endif  
    
#ifndef PWM_CONTROLLER
#define PWM_CONTROLLER (8)  //データ個数2個
#endif  
    
    /* 設定　*/
#define invalid_DATAtype 0

#define recieve_DATAtype 1

#define transmit_DATAtype 2
    
#define superM_recieve_DATAtype 3

#define superM_transmit_DATAtype 4
    
#define superS_recieve_DATAtype transmit_DATAtype

#define superS_transmit_DATAtype recieve_DATAtype    

#ifndef SUCCESSFUL_RS422
#define SUCCESSFUL_RS422 1
#endif    

#ifndef FAILURE_RS422
#define FAILURE_RS422 0
#endif

#ifndef now_idle
#define now_idle 0
#endif

#ifndef now_transmit
#define now_transmit 1
#endif

#ifndef now_recieve
#define now_recieve 2
#endif

#ifndef now_reply
#define now_reply 3
#endif
    
#ifndef GO_mode_mecanum
#define GO_mode_mecanum false
#endif    
    
#ifndef TURN_mode_mecanum
#define TURN_mode_mecanum true
#endif    
    
    
    /* data_idのdefine */
#ifndef Drpm_RS422_id
#define Drpm_RS422_id    0
#endif
    
#ifndef PID_A_gein_RS422_id
#define PID_A_gein_RS422_id  1
#endif

#ifndef PID_B_gein_RS422_id
#define PID_B_gein_RS422_id  2
#endif
    
#ifndef Drpm_up_speed_RS422_id
#define Drpm_up_speed_RS422_id 3
#endif  
    
#ifndef now_Drpm_RS422_id 
#define now_Drpm_RS422_id  4
#endif     
    
#ifndef now_rpm_RS422_id 
#define now_rpm_RS422_id  5
#endif  
    
#ifndef now_duty_RS422_id
#define now_duty_RS422_id  6
#endif   
    
#ifndef duty_servo_RS422_id
#define duty_servo_RS422_id    7
#endif

#ifndef duty_percent_RS422_id
#define duty_percent_RS422_id    8
#endif    

    /* data長のdefine */
#ifndef Drpm_RS422_length
#define Drpm_RS422_length    4
#endif
    
#ifndef PID_gein_RS422_length
#define PID_gein_RS422_length  24
#endif  
    
#ifndef Drpm_up_speed_RS422_length
#define Drpm_up_speed_RS422_length 4
#endif  
    
#ifndef now_Drpm_RS422_length 
#define now_Drpm_RS422_length  4
#endif
    
#ifndef now_rpm_RS422_length 
#define now_rpm_RS422_length  16
#endif  
    
#ifndef now_duty_RS422_length
#define now_duty_RS422_length  16
#endif  
    
#ifndef duty_servo_RS422_length
#define duty_servo_RS422_length    8
#endif

#ifndef duty_percent_RS422_length
#define duty_percent_RS422_length    16
#endif        

    
    /*  data種別のdefine    */
#ifndef Drpm_RS422S_type
#define Drpm_RS422S_type    recieve_DATAtype
#endif
    
#ifndef PID_A_gein_RS422S_type
#define PID_A_gein_RS422S_type  recieve_DATAtype
#endif

#ifndef PID_B_gein_RS422S_type
#define PID_B_gein_RS422S_type recieve_DATAtype
#endif
    
#ifndef Drpm_up_speed_RS422S_type
#define Drpm_up_speed_RS422S_type recieve_DATAtype
#endif  
    
#ifndef now_Drpm_RS422S_type 
#define now_Drpm_RS422S_type  transmit_DATAtype
#endif   
    
#ifndef now_rpm_RS422S_type 
#define now_rpm_RS422S_type  transmit_DATAtype
#endif  
    
#ifndef now_duty_RS422S_type
#define now_duty_RS422S_type  transmit_DATAtype
#endif       
    
#ifndef duty_servo_RS422S_type
#define duty_servo_RS422S_type   recieve_DATAtype
#endif

#ifndef duty_percent_RS422S_type
#define duty_percent_RS422S_type    recieve_DATAtype
#endif   
    
    /*  RS422 マスター  */
    /*      dataID 割り振り     */
#ifndef Drpm_A_RS422M(device_num)
#define Drpm_A_RS422M(device_num)    ((int16_t*)device_id[device_num].data_id[Drpm_RS422_id].data)[0]//16bit
#endif

#ifndef Drpm_B_RS422M(device_num)
#define Drpm_B_RS422M(device_num)    ((int16_t*)device_id[device_num].data_id[Drpm_RS422_id].data)[1]//16bit
#endif

#ifndef P_A_gein_RS422M(device_num)
#define P_A_gein_RS422M(device_num)  ((double*)device_id[device_num].data_id[PID_A_gein_RS422_id].data)[0]
#endif

#ifndef I_A_gein_RS422M(device_num)
#define I_A_gein_RS422M(device_num) ((double*)device_id[device_num].data_id[PID_A_gein_RS422_id].data)[1]
#endif

#ifndef D_A_gein_RS422M(device_num)
#define D_A_gein_RS422M(device_num) ((double*)device_id[device_num].data_id[PID_A_gein_RS422_id].data)[2]
#endif

#ifndef P_B_gein_RS422M(device_num)
#define P_B_gein_RS422M(device_num) ((double*)device_id[device_num].data_id[PID_B_gein_RS422_id].data)[0]
#endif

#ifndef I_B_gein_RS422M(device_num)
#define I_B_gein_RS422M(device_num) ((double*)device_id[device_num].data_id[PID_B_gein_RS422_id].data)[1]
#endif

#ifndef D_B_gein_RS422M(device_num)
#define D_B_gein_RS422M(device_num) ((double*)device_id[device_num].data_id[PID_B_gein_RS422_id].data)[2]
#endif

#ifndef Drpm_A_up_speed_RS422M(device_num)
#define Drpm_A_up_speed_RS422M(device_num) (device_id[device_num].data_id[Drpm_up_speed_RS422_id].data[0])
#endif    

#ifndef Drpm_B_up_speed_RS422M(device_num)
#define Drpm_B_up_speed_RS422M(device_num) (device_id[device_num].data_id[Drpm_up_speed_RS422_id].data[1])
#endif    

#ifndef now_Drpm_A_RS422M(device_num) 
#define now_Drpm_A_RS422M(device_num) ((int16_t*)device_id[device_num].data_id[now_Drpm_RS422_id ].data)[0]//16bit
#endif

#ifndef now_Drpm_B_RS422M(device_num)
#define now_Drpm_B_RS422M(device_num) ((int16_t*)device_id[device_num].data_id[now_Drpm_RS422_id ].data)[1]//32bit
#endif

#ifndef now_rpm_A_RS422M(device_num) 
#define now_rpm_A_RS422M(device_num) ((double*)device_id[device_num].data_id[now_rpm_RS422_id].data)[0]//32bit
#endif  

#ifndef now_rpm_B_RS422M(device_num)
#define now_rpm_B_RS422M(device_num) ((double*)device_id[device_num].data_id[now_rpm_RS422_id].data)[1]//16bit
#endif  

#ifndef now_duty_A_RS422M(device_num)
#define now_duty_A_RS422M(device_num) ((double*)device_id[device_num].data_id[now_duty_RS422_id].data)[0]//16bit
#endif  

#ifndef now_duty_B_RS422M(device_num)
#define now_duty_B_RS422M(device_num) ((double*)device_id[device_num].data_id[now_duty_RS422_id].data)[1]//16bit
#endif
        /*  servo用 */
#ifndef dutyA_servo_RS422M(device_num)
#define dutyA_servo_RS422M(device_num) ((float*)device_id[device_num].data_id[duty_servo_RS422_id].data)[0]//16bit
#endif      
    
#ifndef dutyB_servo_RS422M(device_num)
#define dutyB_servo_RS422M(device_num) ((float*)device_id[device_num].data_id[duty_servo_RS422_id].data)[1]//16bit
#endif    
        /*  単純PWM用   */
#ifndef dutyA_percent_RS422M(device_num)
#define dutyA_percent_RS422M(device_num) ((double*)device_id[device_num].data_id[duty_percent_RS422_id].data)[0]//16bit
#endif      
    
#ifndef dutyB_percent_RS422M(device_num)
#define dutyB_percent_RS422M(device_num) ((double*)device_id[device_num].data_id[duty_percent_RS422_id].data)[1]//16bit
#endif      
    
    
    /*  RS422 スレーブ*/
    /*      dataID 割り振り     */
#ifndef Drpm_A_RS422S
#define Drpm_A_RS422S    ((int16_t*)data_id_S[Drpm_RS422_id].data)[0]//16bit
#endif

#ifndef Drpm_B_RS422S
#define Drpm_B_RS422S    ((int16_t*)data_id_S[Drpm_RS422_id].data)[1]//16bit
#endif

#ifndef P_A_gein_RS422S
#define P_A_gein_RS422S  ((double*)data_id_S[PID_A_gein_RS422_id].data)[0]
#endif

#ifndef I_A_gein_RS422S
#define I_A_gein_RS422S ((double*)data_id_S[PID_A_gein_RS422_id].data)[1]
#endif

#ifndef D_A_gein_RS422S
#define D_A_gein_RS422S ((double*)data_id_S[PID_A_gein_RS422_id].data)[2]
#endif

#ifndef P_B_gein_RS422S
#define P_B_gein_RS422S ((double*)data_id_S[PID_B_gein_RS422_id].data)[0]
#endif

#ifndef I_B_gein_RS422S
#define I_B_gein_RS422S ((double*)data_id_S[PID_B_gein_RS422_id].data)[1]
#endif

#ifndef D_B_gein_RS422S
#define D_B_gein_RS422S ((double*)data_id_S[PID_B_gein_RS422_id].data)[2]
#endif

#ifndef Drpm_A_up_speed_RS422S
#define Drpm_A_up_speed_RS422S (((int8_t*)data_id_S[Drpm_up_speed_RS422_id].data)[0])
#endif    

#ifndef Drpm_B_up_speed_RS422S
#define Drpm_B_up_speed_RS422S (((int8_t*)data_id_S[Drpm_up_speed_RS422_id].data)[1])
#endif    

#ifndef now_Drpm_A_RS422S 
#define now_Drpm_A_RS422S ((int16_t*)data_id_S[now_Drpm_RS422_id ].data)[0]//16bit
#endif

#ifndef now_Drpm_B_RS422S
#define now_Drpm_B_RS422S ((int16_t*)data_id_S[now_Drpm_RS422_id ].data)[1]//32bit
#endif

#ifndef now_rpm_A_RS422S 
#define now_rpm_A_RS422S ((double*)data_id_S[now_rpm_RS422_id].data)[0]//32bit
#endif  

#ifndef now_rpm_B_RS422S
#define now_rpm_B_RS422S ((double*)data_id_S[now_rpm_RS422_id].data)[1]//16bit
#endif  

#ifndef now_duty_A_RS422S
#define now_duty_A_RS422S ((double*)data_id_S[now_duty_RS422_id].data)[0]//16bit
#endif  

#ifndef now_duty_B_RS422S
#define now_duty_B_RS422S ((double*)data_id_S[now_duty_RS422_id].data)[1]//16bit
#endif
        /*  servo用 */
#ifndef dutyA_servo_RS422S
#define dutyA_servo_RS422S ((uint16_t*)data_id_S[duty_servo_RS422_id].data)[0]//16bit
#endif      
    
#ifndef dutyB_servo_RS422S
#define dutyB_servo_RS422S ((uint16_t*)data_id_S[duty_servo_RS422_id].data)[1]//16bit
#endif    
        /*  単純PWM用   */
    
#ifndef dutyA_percent_RS422S
#define dutyA_percent_RS422S ((double*)data_id_S[duty_percent_RS422_id].data)[0]//16bit
#endif      
    
#ifndef dutyB_percent_RS422S
#define dutyB_percent_RS422S ((double*)data_id_S[duty_percent_RS422_id].data)[1]//16bit
#endif      
    
    /* superRS422*/
#define API_StartData (0x7E)
    
    
    
#ifdef	__cplusplus
}
#endif

#endif	/* DEFINES_FOR_RS422_H */

