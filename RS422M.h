/* 
 * File:   RS422M.h
 * Author: Shuuya
 *
 * Created on 2016/03/08, 1:25
 */

#ifndef RS422M_H
#define	RS422M_H
//データベースの作成とrs422の通信のヘッダファイル

#include <xc.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <libpic30.h>
#include "dsPIC33FJ128_head.h"
#include "defines_for_RS422.h"

#ifdef	__cplusplus
extern "C" {
#endif

    /*使用マイコン固有レジスタのマクロ*/

#ifndef RS422_bps
#define RS422_bps UART1_bps
#endif

#ifndef RS422_TXREG
#define RS422_TXREG U1TXREG
#endif

#ifndef RS422_RXREG
#define RS422_RXREG U1RXREG
#endif

#ifndef RS422_TXIF
#define RS422_TXIF IFS0bits.U1TXIF
#endif

#ifndef RS422_TXIE
#define RS422_TXIE IEC0bits.U1TXIE
#endif
   
#ifndef RS422_TXIP
#define RS422_TXIP IPC3bits.U1TXIP
#endif    

#ifndef RS422_RXIF
#define RS422_RXIF IFS0bits.U1RXIF
#endif  
    
#ifndef RS422_ADDEN
#define RS422_ADDEN U1STAbits.ADDEN
#endif  

#ifndef RS422_TMR_ON_nOFF
#define RS422_TMR_ON_nOFF T5CONbits.TON
#endif    

#ifndef RS422_TMR
#define RS422_TMR TMR5
#endif

#ifndef MAX_TMR_COUNT
#define MAX_TMR_COUNT 65535.0
#endif

#ifndef RS422_TMR_TCKPS
#define RS422_TMR_TCKPS T5CONbits.TCKPS
#endif      

#ifndef RS422_TMRIE
#define RS422_TMRIE IEC1bits.T5IE
#endif    

#ifndef RS422_TMRIF
#define RS422_TMRIF IFS1bits.T5IF
#endif
    
#ifndef RS422_TMRIP
#define RS422_TMRIP IPC7bits.T5IP//優先度
#endif

    
    /*データベースの設定*/
#ifndef MAX_DATA_SIZE
#define MAX_DATA_SIZE 24
#endif

#ifndef MAX_DATA_KINDS
#define MAX_DATA_KINDS 16
#endif

#ifndef MAX_DEVICE_KINDS
#define MAX_DEVICE_KINDS 16
#endif

#ifndef BUFFER_SIZE
#define BUFFER_SIZE (3+(MAX_DATA_SIZE))
#endif
    //deviceID,dataID,length,datas


#ifndef delay_RS422
#define delay_RS422 (MAX_TMR_COUNT-((((1.0/(RS422_bps))* 96.0 )*Fcyc) / 8.0))//96bit分の間隔を作成
#endif 

    /*マクロ関数*/
    
    //#define set_data_type(deviceID_num,dataID_num,data_type) device_id[ deviceID_num ].data_id[ dataID_num ].status.type= data_type ;
    //データ群のデータタイプを指定する関数,引数：(deviceID,dataID,指定するdataタイプ)
#define set_ACK_RS422()     {device_id[search_deviceID].data_id[search_dataID].status.now_RS422 = now_reply;\
                            RS422_TXIF   = 0;\
                            putUART_RS422(6, UART_data_type);\
                            RS422_TXIE   = 1;\
                            now_buffer_position = 0;\
                            }
    
#define set_now_mes_again() {putUART_RS422(buffer_422M[0], UART_add_type); now_buffer_position = 1;}
#define search_deviceID     (index_valid[ now_rs422_position ].number_device_ID)    //現在参照している有効deviceID
#define search_dataID       (index_valid[ now_rs422_position ].number_data_ID)      //現在参照している有効dataID
    
#define putUART_RS422(datas,typeUART) {\
    RS422_TXIF = 0;\
    if (typeUART == UART_data_type) {\
        RS422_TXREG = (((uint16_t)datas) & 0x00FF);\
    } else {\
        RS422_TXREG = (((uint16_t)datas) | 0x0100);\
    }\
}    

#define tmr_setup_RS422() {device_id[search_deviceID].data_id[search_dataID].status.now_RS422 = now_idle;\
                           RS422_TXIE = 0;RS422_TMRIP = TMR5_IP;\
                           RS422_TMRIF = 0;RS422_TMRIE = 1;\
                           RS422_TMR_TCKPS = 1;\
                           if(( delay_RS422 ) < 0 ) {RS422_TMR = delay_RS422;}\
                           else{RS422_TMR=0 ;}\
                           RS422_TMR_ON_nOFF = 1;}

#define tmr_ISR_RS422() {RS422_TMRIE = 0;\
                        RS422_TMRIF = 0;\
                        RS422_TMR_ON_nOFF = 0;\
                        now_buffer_position = 0;\
                        set_next_mes();}//タイマー割り込み関数にて呼び出すべき関数(送信間隔設定関数)

    typedef union {
        uint8_t byte;

        struct {
            unsigned type : 2; //(dataの型)
            unsigned CHECK : 1; //通信の状態（成功か失敗か）
            unsigned now_RS422 : 3; //現在の通信状態
            unsigned OTHER : 2;
        };

    } data_status;

    typedef struct msgstr {
        uint8_t data[MAX_DATA_SIZE]; //(有効な命令もしくはデータの配列) ※宣言時、配列[n]={};ですべて0になります（知らんかった）
        uint8_t length; //dataの配列個数
        data_status status; //(受信データ=0　or 送信データ=1 or 無効なデータ=2)
    } rs422_data; //0~F:モータコントローラ

    typedef struct msgtxt {
        rs422_data data_id[MAX_DATA_KINDS];
    } rs422msg;

    typedef struct msgxy {
        uint8_t number_device_ID;
        uint8_t number_data_ID;
    } rs422_data_position;


    extern uint8_t now_buffer_position;
    extern uint16_t now_rs422_position;
    extern uint16_t number_of_valid_data; //有効データ数 ※データが超膨大でないことを仮定(最大:short:32台,64個の32byteの配列)
    extern uint8_t buffer_422M[BUFFER_SIZE];
    extern rs422msg device_id[MAX_DEVICE_KINDS];
    extern rs422_data_position index_valid[(MAX_DEVICE_KINDS)*(MAX_DATA_KINDS)];
    
    void Start_RS422(); //初めに呼び出すべき関数(初期化関数)
    void set_next_mes(); //次の送信データを準備する関数だが、プログラマは呼び出す必要はない
    void rx_ISR_RS422(); //受信割り込み関数にて呼び出すべき関数(定期通信データベース化関数)
    void tx_ISR_RS422(); //送信割り込み関数にて呼び出すべき関数（連続送信関数）
    
/***************************************************************************************/
    /*EEPROM遠隔書き込み用 RS422とは違うUARTを用いる*/
/***************************************************************************************/
    
#ifndef toEEPROM_TXREG
#define toEEPROM_TXREG U2TXREG
#endif

#ifndef toEEPROM_RXREG
#define toEEPROM_RXREG U2RXREG
#endif

#ifndef toEEPROM_TXIF
#define toEEPROM_TXIF IFS1bits.U2TXIF
#endif

#ifndef toEEPROM_TXIE
#define toEEPROM_TXIE IEC1bits.U2TXIE
#endif

#ifndef toEEPROM_RXIF
#define toEEPROM_RXIF IFS1bits.U2RXIF
#endif  
    
#ifndef toEEPROM_RXIE
#define toEEPROM_RXIE IEC1bits.U2RXIE
#endif  
    
    
    typedef struct msgtoEEPROM {
        uint8_t device_id;
        uint8_t data_id;
        uint8_t length;
        uint8_t data[16];
    } rs422_data_EEPROM;//EEPROM
    
#define I2CM_ISR_toEEPROM() {IFS1bits.MI2C1IF=0;\
    if(status_I2C==0){\
        while(writeEEPROM_RS422M() == false);\
    }else if(status_I2C==1){\
        while(readEEPROM_RS422M() == false);\
    }}//I2Cマスタの割り込み関数で使用してください
    
    extern rs422_data_EEPROM* datas_write;
    extern rs422_data* datas_read;
    extern uint8_t status_I2C;
    extern uint8_t adress_device,adress_data;
    extern rs422_data_EEPROM buffer_EEPROM[2];//buffer_EEPROM[0]:書き込み用バッファ,buffer_EEPROM[1]:読み込み用バッファ
    
    bool writeEEPROM_RS422M(void);
    bool readEEPROM_RS422M (void);
    
    void rx_ISR_toEEPROM(void);//パソコンからEEPROMへの書き込みデータ受信関数(プロトコルはRS422と同じ)

#ifdef	__cplusplus
}
#endif

#endif	/* RS422M_H */

