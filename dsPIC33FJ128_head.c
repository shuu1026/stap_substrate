#define FCY (10000000)

#include <xc.h>    
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <libpic30.h>
#include <stdbool.h>
#include <math.h>

#include "dsPIC33FJ128_head.h"

uint16_t T2CKPS = 0;
uint16_t T3CKPS = 0;
uint8_t Controller_variables[7] = {};

void setup_ports() {

    uint8_t* pointer = &(((uint8_t*) & RPOR0)[0]);
    uint16_t process = 0;

    if ((UART1_bps > 0)&&(UART1_PIN_TX != not_used_module)&&(UART1_PIN_RX != not_used_module)) {
        TRISB = (TRISB & ((1 << UART1_PIN_TX) ^ 0xFFFF)); //ピン設定
        TRISB = (TRISB | (1 << UART1_PIN_RX));
        RPINR18bits.U1RXR = UART1_PIN_RX;
        pointer[UART1_PIN_TX] = 3;
        process = process | (1 << UART1_PIN_RX);
    }

    if ((UART2_bps > 0) && (UART2_PIN_TX != not_used_module) && (UART2_PIN_RX != not_used_module)) {
        TRISB = (TRISB & ((1 << UART2_PIN_TX) ^ 0xFFFF)); //ピン設定
        TRISB = (TRISB | (1 << UART2_PIN_RX));
        RPINR19bits.U2RXR = UART2_PIN_RX;
        pointer[UART2_PIN_TX] = 5;
        process = process | (1 << UART2_PIN_RX);
    }

    if ((OC_1and2_period > 0) && (OC1_PIN != not_used_module)) {
        TRISB = (TRISB & ((1 << OC1_PIN) ^ 0xFFFF)); //ピン設定
        pointer[OC1_PIN] = 0x12;
    }

    if ((OC_1and2_period > 0) && (OC2_PIN != not_used_module)) {
        TRISB = (TRISB & ((1 << OC2_PIN) ^ 0xFFFF)); //ピン設定
        pointer[OC2_PIN] = 0x13; //ピン設定
    }

    if ((OC_3and4_period > 0) && (OC3_PIN != not_used_module)) {
        TRISB = (TRISB & ((1 << OC3_PIN) ^ 0xFFFF)); //ピン設定
        pointer[OC3_PIN] = 0x14; //ピン設定 
    }

    if ((OC_3and4_period > 0) && (OC4_PIN != not_used_module)) {
        TRISB = (TRISB & ((1 << OC4_PIN) ^ 0xFFFF)); //ピン設定
        pointer[OC4_PIN] = 0x15; //ピン設定
    }

    if ((QEI1_PIN_A != not_used_module) && (QEI1_PIN_B != not_used_module)) {
        TRISB = (TRISB | (1 << QEI1_PIN_A));
        RPINR14bits.QEA1R = QEI1_PIN_A;
        process = process | (1 << QEI1_PIN_A);
        TRISB = (TRISB | (1 << QEI1_PIN_B));
        RPINR14bits.QEB1R = QEI1_PIN_B;
        process = process | (1 << QEI1_PIN_B);
    }

    if ((QEI2_PIN_A != not_used_module) && (QEI2_PIN_B != not_used_module)) {
        TRISB = (TRISB | (1 << QEI2_PIN_A));
        RPINR16bits.QEA2R = QEI2_PIN_A;
        process = process | (1 << QEI2_PIN_A);
        TRISB = (TRISB | (1 << QEI2_PIN_B));
        RPINR16bits.QEB2R = QEI2_PIN_B;
        process = process | (1 << QEI2_PIN_B);
    }

    TRISB = TRISB & process;

    return;
}

void setupUART(void) {

    if ((UART1_bps > 0)&&(UART1_PIN_TX != not_used_module)&&(UART1_PIN_RX != not_used_module)) {

        U1MODE = 0;
        U1STA = 0;
        U1BRG = 0;

        U1MODEbits.BRGH = 1;
        PMD1bits.U1MD = 0;
        U1BRG = (((Fcyc) / (UART1_bps * 4)) - 1);
        //U1STAbits.UTXINV = 1;
        //U1MODEbits.URXINV = 1;

        if (UART1_type == nine_bitsUARTtype) {
            U1MODEbits.PDSEL = 3; //9bit
        } else {
            U1MODEbits.PDSEL = 0;
        }
        U1MODEbits.STSEL = 0;

        if (UART1TX_IP == 0) {
            IEC0bits.U1TXIE = 0;
            IPC3bits.U1TXIP = 0;
            IFS0bits.U1TXIF = 0;
        } else if (UART1TX_IP < 7) {
            IEC0bits.U1TXIE = 1;
            IPC3bits.U1TXIP = UART1TX_IP;
        } else {
            IEC0bits.U1TXIE = 1;
            IPC3bits.U1TXIP = 7;
        }

        if (UART1RX_IP == 0) {
            IEC0bits.U1RXIE = 0;
            IPC2bits.U1RXIP = 0;
            IFS0bits.U1RXIF = 0;
        } else if (UART1RX_IP < 7) {
            IEC0bits.U1RXIE = 1;
            IPC2bits.U1RXIP = UART1RX_IP;
            IFS0bits.U1RXIF = 0;
        } else {
            IEC0bits.U1RXIE = 1;
            IPC2bits.U1RXIP = 7;
            IFS0bits.U1RXIF = 0;
        }

        U1MODEbits.RTSMD = 0;
        U1STAbits.UTXISEL0 = 1;
        U1STAbits.UTXISEL1 = 0;

        U1MODEbits.UARTEN = 1;
        U1STAbits.UTXEN = 1;
        U1MODEbits.UEN = 0; //TX1,RX1


    }
    if ((UART2_bps > 0) && (UART2_PIN_TX != not_used_module) && (UART2_PIN_RX != not_used_module)) {

        U2MODE = 0;
        U2STA = 0;
        U2BRG = 0;

        U2MODEbits.BRGH = 1; //1:高速モード
        PMD1bits.U2MD = 0;
        U2BRG = (((Fcyc) / (UART2_bps * 4)) - 1);
        if (UART2_type == nine_bitsUARTtype) {
            U2MODEbits.PDSEL = 3; //9bit
        } else {
            U2MODEbits.PDSEL = 0;
        }
        U2MODEbits.STSEL = 0;

        if (UART2TX_IP == 0) {
            IEC1bits.U2TXIE = 0;
            IPC7bits.U2TXIP = 0;
            IFS1bits.U2TXIF = 0;
        } else if (UART2TX_IP < 7) {
            IEC1bits.U2TXIE = 1;
            IPC7bits.U2TXIP = UART2TX_IP;
            IFS1bits.U2TXIF = 0;
        } else {
            IEC1bits.U2TXIE = 1;
            IPC7bits.U2TXIP = 7;
            IFS1bits.U2TXIF = 0;
        }

        if (UART2RX_IP == 0) {
            IEC1bits.U2RXIE = 0;
            IPC7bits.U2RXIP = 0;
            IFS1bits.U2RXIF = 0;
        } else if (UART2RX_IP < 7) {
            IEC1bits.U2RXIE = 1;
            IPC7bits.U2RXIP = UART2RX_IP;
            IFS1bits.U2RXIF = 0;
        } else {
            IEC1bits.U2RXIE = 1;
            IPC7bits.U2RXIP = 7;
            IFS1bits.U2RXIF = 0;
        }

        U2MODEbits.RTSMD = 0;
        U2STAbits.UTXISEL0 = 0;
        U2STAbits.UTXISEL1 = 0;

        U2MODEbits.UARTEN = 1;
        U2STAbits.UTXEN = 1;
        U2MODEbits.UEN = 0; //TX2,RX2

    }
    __delay_ms(1);
    return;
}

void setupOC(void) {


    if (OC_1and2_period > 0) {
        OC1R = 0;
        OC1RS = 0;
        OC1CON = 0;
        OC2R = 0;
        OC2RS = 0;
        OC2CON = 0;
        T2CON = 0;
        if (OC_1and2_period < 1600) {
            T2CKPS = 1;
            PR2 = ((OC_1and2_period * micro * Fcyc) / (1));
            T2CONbits.TCKPS = 0;
        } else if (OC_1and2_period < 13000) {
            T2CKPS = 8;
            PR2 = ((OC_1and2_period * micro * Fcyc) / (8));
            T2CONbits.TCKPS = 1;
        } else if (OC_1and2_period < 100000) {
            T2CKPS = 64;
            PR2 = ((OC_1and2_period * micro * Fcyc) / (64));
            T2CONbits.TCKPS = 2;
        } else if (OC_1and2_period < 838848) {
            T2CKPS = 256;
            PR2 = ((OC_1and2_period * micro * Fcyc) / (256));
            T2CONbits.TCKPS = 3;
        } else {
            T2CKPS = 256;
            PR2 = 0xFFFF;
            T2CONbits.TCKPS = 3;
        }
        PR2 = (((OC_1and2_period) * (micro) * (Fcyc)) / (T2CKPS));
        if (OC1_PIN != not_used_module) {

            PMD2bits.OC1MD = 0;
            OC1CONbits.OCM = 6;
            OC1CONbits.OCTSEL = 0;
            T2CONbits.TON = 1;
        }

        if (OC2_PIN != not_used_module) {

            PMD2bits.OC2MD = 0;
            OC2CONbits.OCM = 6;
            OC2CONbits.OCTSEL = 0;
            T2CONbits.TON = 1;
        }

    }
    if (OC_3and4_period > 0) {
        OC3R = 0;
        OC3RS = 0;
        OC3CON = 0;
        OC4R = 0;
        OC4RS = 0;
        OC4CON = 0;
        T3CON = 0;
        if (OC_3and4_period < 1600) {
            T3CKPS = 1;
            PR2 = ((OC_3and4_period * micro * Fcyc) / (1));
            T3CONbits.TCKPS = 0;
        } else if (OC_3and4_period < 13000) {
            T3CKPS = 8;
            PR3 = ((OC_3and4_period * micro * Fcyc) / (8));
            T3CONbits.TCKPS = 1;
        } else if (OC_3and4_period < 100000) {
            T3CKPS = 64;
            PR3 = ((OC_3and4_period * micro * Fcyc) / (64));
            T3CONbits.TCKPS = 2;
        } else if (OC_3and4_period < 838848) {
            T3CKPS = 256;
            PR3 = ((OC_3and4_period * micro * Fcyc) / (256));
            T3CONbits.TCKPS = 3;
        } else {
            T3CKPS = 256;
            PR3 = 0xFFFF;
            T3CONbits.TCKPS = 3;
        }
        PR3 = (((OC_3and4_period) * (micro) * (Fcyc)) / (T3CKPS));
        if (OC3_PIN != not_used_module) {

            PMD2bits.OC3MD = 0;
            OC3CONbits.OCM = 6;
            OC3CONbits.OCTSEL = 1; //1:TMR3をタイマーベースとする
            T3CONbits.TON = 1;
        }
        if (OC4_PIN != not_used_module) {

            PMD2bits.OC3MD = 0;
            OC4CONbits.OCM = 6;
            OC4CONbits.OCTSEL = 1;
            T3CONbits.TON = 1;
        }

    }

    return;
}

void setduty_OC(unsigned char Move_Number_of_OC, bool pinfunction, int32_t duty) {

    if (Move_Number_of_OC == 1) {
        if (pinfunction == active_HI) {
            OC1RS = ((uint16_t) fabs(duty));
        } else {
            OC1RS = PR2 - ((uint16_t) fabs(duty));
        }
    } else if (Move_Number_of_OC == 2) {
        if (pinfunction == active_HI) {
            OC2RS = ((uint16_t) fabs(duty));
        } else {
            OC2RS = PR2 - ((uint16_t) fabs(duty));
        }
    } else if (Move_Number_of_OC == 3) {
        if (pinfunction == active_HI) {
            OC3RS = ((uint16_t) fabs(duty));
        } else {
            OC3RS = PR3 - ((uint16_t) fabs(duty));
        }
    } else {
        if (pinfunction == active_HI) {
            OC4RS = ((uint16_t) fabs(duty));
        } else {
            OC4RS = PR3 - ((uint16_t) fabs(duty));
        }
    }

    return;

}

void setdutySec_for_OC(unsigned char Move_Number_of_OC, bool pinfunction, unsigned short MicroSec) {

    if (Move_Number_of_OC == 1) {
        if (pinfunction == true) {
            OC1RS = ((MicroSec * micro * Fcyc) / (T2CKPS));
        } else {
            OC1RS = PR2 - ((MicroSec * micro * Fcyc) / (T2CKPS));
        }
    } else if (Move_Number_of_OC == 2) {
        if (pinfunction == true) {
            OC2RS = ((MicroSec * micro * Fcyc) / (T2CKPS));
        } else {
            OC2RS = PR2 - ((MicroSec * micro * Fcyc) / (T2CKPS));
        }
    } else if (Move_Number_of_OC == 3) {
        if (pinfunction == true) {
            OC3RS = ((MicroSec * micro * Fcyc) / (T3CKPS));
        } else {
            OC3RS = PR3 - ((MicroSec * micro * Fcyc) / (T3CKPS));
        }
    } else {
        if (pinfunction == true) {
            OC4RS = ((MicroSec * micro * Fcyc) / (T3CKPS));
        } else {
            OC4RS = PR3 - ((MicroSec * micro * Fcyc) / (T3CKPS));
        }
    }

    return;
}

void setupTMR(void) {

    T1CON = 0;
    TMR1 = 0;
    T2CON = 0;
    TMR2 = 0;
    T3CON = 0;
    TMR3 = 0;
    T4CON = 0;
    TMR4 = 0;
    T5CON = 0;
    TMR5 = 0;

    if (TMR1_IP != not_used_module) {
        if (TMR1_IP == 0) {
            IEC0bits.T1IE = 0;
            IPC0bits.T1IP = 0;
            IFS0bits.T1IF = 0;
        } else if (TMR1_IP < 7) {
            IEC0bits.T1IE = 1;
            IPC0bits.T1IP = TMR1_IP;
            IFS0bits.T1IF = 0;
        } else {
            IEC0bits.T1IE = 1;
            IPC0bits.T1IP = 7;
            IFS0bits.T1IF = 0;
        }
    }

    if (TMR2_IP != not_used_module) {
        if (TMR2_IP == 0) {
            IEC0bits.T2IE = 0;
            IPC1bits.T2IP = 0;
            IFS0bits.T2IF = 0;
        } else if (TMR2_IP < 7) {
            IEC0bits.T2IE = 1;
            IPC1bits.T2IP = TMR2_IP;
            IFS0bits.T2IF = 0;
        } else {
            IEC0bits.T2IE = 1;
            IPC1bits.T2IP = 7;
            IFS0bits.T2IF = 0;
        }
    }

    if (TMR3_IP != not_used_module) {
        if (TMR3_IP == 0) {
            IEC0bits.T3IE = 0;
            IPC2bits.T3IP = 0;
            IFS0bits.T3IF = 0;
        } else if (TMR3_IP < 7) {
            IEC0bits.T3IE = 1;
            IPC2bits.T3IP = TMR3_IP;
            IFS0bits.T3IF = 0;
        } else {
            IEC0bits.T3IE = 1;
            IPC2bits.T3IP = 7;
            IFS0bits.T3IF = 0;
        }
    }

    if (TMR4_IP != not_used_module) {
        if (TMR4_IP == 0) {
            IEC1bits.T4IE = 0;
            IPC6bits.T4IP = 0;
            IFS1bits.T4IF = 0;
        } else if (TMR4_IP < 7) {
            IEC1bits.T4IE = 1;
            IPC6bits.T4IP = TMR4_IP;
            IFS1bits.T4IF = 0;
        } else {
            IEC1bits.T4IE = 1;
            IPC6bits.T4IP = 7;
            IFS1bits.T4IF = 0;
        }
    }

    if (TMR5_IP != not_used_module) {
        if (TMR5_IP == 0) {
            IEC1bits.T5IE = 0;
            IPC7bits.T5IP = 0;
            IFS1bits.T5IF = 0;
        } else if (TMR5_IP < 7) {
            IEC1bits.T5IE = 1;
            IPC7bits.T5IP = TMR5_IP;
            IFS1bits.T5IF = 0;
        } else {
            IEC1bits.T5IE = 1;
            IPC7bits.T5IP = 7;
            IFS1bits.T5IF = 0;
        }
    }

    return;
}

void setdelay(unsigned char move_nomber_of_TMR, unsigned char mode, double Sec) {

    unsigned long TMR;
    if (mode == 0) {
        TMR = 0xFFFF - ((Sec * milli * Fcyc) / (256));
    } else if (mode == 1) {
        TMR = 0xFFFF - ((Sec * micro * Fcyc) / (8));
    } else if (mode == 2) {
        TMR = 0xFFFF - (Sec * nano * Fcyc);
    } else {
        TMR = 0;
    }

    if (move_nomber_of_TMR == 1) {
        if (mode == 0) {
            T1CONbits.TCKPS = 3;
        } else if (mode == 1) {
            T1CONbits.TCKPS = 1;
        } else if (mode == 2) {
            T1CONbits.TCKPS = 0;
        }
        TMR1 = TMR;
        IFS0bits.T1IF = 0;
        T1CONbits.TON = 1;
        return;
    } else if (move_nomber_of_TMR == 2) {
        if (mode == 0) {
            T2CONbits.TCKPS = 3;
        } else if (mode == 1) {
            T2CONbits.TCKPS = 1;
        } else if (mode == 2) {
            T2CONbits.TCKPS = 0;
        }
        TMR2 = TMR;
        IFS0bits.T2IF = 0;
        T2CONbits.TON = 1;
        return;
    } else if (move_nomber_of_TMR == 3) {
        if (mode == 0) {
            T3CONbits.TCKPS = 3;
        } else if (mode == 1) {
            T3CONbits.TCKPS = 1;
        } else if (mode == 2) {
            T3CONbits.TCKPS = 0;
        }
        TMR3 = TMR;
        IFS0bits.T3IF = 0;
        T3CONbits.TON = 1;
        return;
    } else if (move_nomber_of_TMR == 4) {
        if (mode == 0) {
            T4CONbits.TCKPS = 3;
        } else if (mode == 1) {
            T4CONbits.TCKPS = 1;
        } else if (mode == 2) {
            T4CONbits.TCKPS = 0;
        }
        TMR4 = TMR;
        IFS1bits.T4IF = 0;
        T4CONbits.TON = 1;
        return;
    } else if (move_nomber_of_TMR == 5) {
        if (mode == 0) {
            T5CONbits.TCKPS = 3;
        } else if (mode == 1) {
            T5CONbits.TCKPS = 1;
        } else if (mode == 2) {
            T5CONbits.TCKPS = 0;
        }
        TMR5 = TMR;
        IFS1bits.T5IF = 0;
        T5CONbits.TON = 1;
        return;
    }
    return;
}

void setupQEI(void) {

    if ((QEI1_PIN_A != not_used_module) && (QEI1_PIN_B != not_used_module)) {

        if (QEI1_IP != not_used_module) {
            if (QEI1_IP == 0) {
                IEC3bits.QEI1IE = 0;
                IPC14bits.QEI1IP = 0;
                IFS3bits.QEI1IF = 0;
            } else if (QEI1_IP < 7) {
                IEC3bits.QEI1IE = 1;
                IPC14bits.QEI1IP = QEI1_IP;
                IFS3bits.QEI1IF = 0;
            } else {
                IEC3bits.QEI1IE = 1;
                IPC14bits.QEI1IP = 7;
                IFS3bits.QEI1IF = 0;
            }
        }//割り込み設定

        POS1CNT = 0;
        MAX1CNT = 0xFFFF;
        QEI1CON = 0;
        PMD1bits.QEI1MD = 0;
        if (QEI1_type == speed_control) {
            QEI1CONbits.QEIM = 7;
        } else {
            QEI1CONbits.QEIM = 4;
        }
        QEI1CONbits.PCDOUT = 0;

    }
    if ((QEI2_PIN_A != not_used_module) && (QEI2_PIN_B != not_used_module)) {

        if (QEI2_IP != not_used_module) {
            if (QEI2_IP == 0) {
                IEC4bits.QEI2IE = 0;
                IPC18bits.QEI2IP = 0;
                IFS4bits.QEI2IF = 0;
            } else if (QEI2_IP < 7) {
                IEC4bits.QEI2IE = 1;
                IPC18bits.QEI2IP = QEI1_IP;
                IFS4bits.QEI2IF = 0;
            } else {
                IEC4bits.QEI2IE = 1;
                IPC18bits.QEI2IP = 7;
                IFS4bits.QEI2IF = 0;
            }
        }//割り込み設定

        POS2CNT = 0;
        MAX2CNT = 0xFFFF;
        QEI2CON = 0;
        PMD3bits.QEI2MD = 0;
        if (QEI2_type == speed_control) {
            QEI2CONbits.QEIM = 7;
        } else {
            QEI2CONbits.QEIM = 4;
        }
        QEI2CONbits.PCDOUT = 0;

    }

    return;
}

bool check_data_Controler(uint8_t* data, uint8_t data_num) {
    //data 確認するデータ
    //data_num　現在データ番号

    /*  S   */
    if (data_num == 0) {
        return (data[data_num] == 'S');
    }
    /*　E   */
    if (data_num == 18) {
        if (data[data_num] == 'E') {
            return true;
        } else {
            return false;
        }
    }
    /*  2回目 SもしくはE*/
    if ((data_num == 1) || (data_num == 19)) {
        return (data[data_num] == data[data_num - 1]); //２回目のSもしくはEのとき
    }
    /*  実データ部  */
    if (data_num % 2 == 0) {
        return true; //偶数回目の受信
    } else {
        return (data[data_num] == (data[data_num - 1] ^ 0xFF));
    }//奇数回目で偶数回目の反転

    return false;
}

bool Controler_checker_ISR(uint8_t using_UART) {

    static Controler_status status;
    static uint8_t direct_data[18];
    uint8_t n, i;
    /*
    uint16_t* continue_read = &U1STA;
    if (using_UART == 1) {
        n = 0;
    } else {
        n = 1;
        for (i = 0; i < using_UART - 1; i++) {
            n = n * (i + 1);
        }
    }
    n = n * 16;
    
    continue_read = continue_read + n;*/

    while (U1STAbits.URXDA == 1) {
        if (using_UART == 1) {
            direct_data[status.recievetime] = U1RXREG;
            _U1RXIF = 0;
        } else if (using_UART == 2) {
            direct_data[status.recievetime] = U2RXREG;
            _U2RXIF = 0;
        }
        if (check_data_Controler(direct_data, status.recievetime) == true) {
            if (status.recievetime >= 19) {
                status.recievetime = 0;
                for (i = 0; i < 8; i++) {
                    Controller_variables[i] = direct_data[(i * 2) + 2];
                }
            } else {
                status.recievetime++;
            }
            status.useful = true;
        } else {
            status.useful = false;
            status.recievetime = 0;
        }
    }
    return status.useful;
}

unsigned char makeASCII(unsigned char result) {

    if (result <= 9) {
        result = result + 48;
    } else {
        result = result + 55;
    }

    return result;

}

void makeASCIIs(unsigned const char mode, signed short result, unsigned char* returns) {

    if (result > 0) {
        returns[0] = 43;
    } else {
        returns[0] = 45;
    }

    result = abs(result);

    if (mode == 16) {
        returns[4] = result & 0x000F;
        returns[4] = makeASCII(returns[4]);

        returns[3] = (result >> 4)& 0x000F;
        returns[3] = makeASCII(returns[3]);

        returns[2] = (result >> 8)& 0x000F;
        returns[2] = makeASCII(returns[2]);

        returns[1] = (result >> 12)& 0x000F;
        returns[1] = makeASCII(returns[1]);

        returns[5] = 32; //スペース

    } else if (mode == 10) {
        returns[1] = (result / 10000);
        returns[2] = (result / 1000)-(returns[1]*10);
        returns[3] = (result / 100)-(returns[2]*10 + returns[1]*100);
        returns[4] = (result / 10)-(returns[3]*10 + returns[2]*100 + returns[1]*1000);
        returns[5] = result - (returns[4]*10 + returns[3]*100 + returns[2]*1000 + returns[1]*10000);

        returns[1] = makeASCII(returns[1]);
        returns[2] = makeASCII(returns[2]);
        returns[3] = makeASCII(returns[3]);
        returns[4] = makeASCII(returns[4]);
        returns[5] = makeASCII(returns[5]);

    }

    returns[6] = 32; //スペース

    return;
}

void tmr_ISR_PID_Control(PID_variables* elements) {

    /*固定小数点の使用やPIDの計算式の改良をするべき*/

    static double last_dutyA = 0.0, last_dutyB = 0.0;
    static double ErrorA1 = 0.0, ErrorA2 = 0.0, ErrorA3 = 0.0;
    static double ErrorB1 = 0.0, ErrorB2 = 0.0, ErrorB3 = 0.0;
#if 0
    /*Drpmの上昇*/
    if (((*elements->DrpmA)>(*elements->now_DrpmA)) && (labs(*elements->DrpmA - *elements->now_DrpmA)>(*elements->DrpmA_up_speed))) {
        *elements->now_DrpmA = ((*elements->now_DrpmA)+(((double) *elements->DrpmA_up_speed) * (QEI_sampling_Sec)));
    } else if (((*elements->DrpmA)<(*elements->now_DrpmA)) && (labs(*elements->DrpmA - *elements->now_DrpmA)>(*elements->DrpmA_up_speed))) {
        *elements->now_DrpmA = ((*elements->now_DrpmA)+((((double) *elements->DrpmA_up_speed) * (-1.0)) * (QEI_sampling_Sec)));
    } else {
        *elements->now_DrpmA = *elements->DrpmA;
    }

    if (((*elements->DrpmB)>(*elements->now_DrpmB)) && (labs(*elements->DrpmB - *elements->now_DrpmB)>(*elements->DrpmB_up_speed))) {
        *elements->now_DrpmB = ((*elements->now_DrpmB)+(((double) *elements->DrpmB_up_speed) * (QEI_sampling_Sec)));
    } else if (((*elements->DrpmB)<(*elements->now_DrpmB)) && (labs(*elements->DrpmB - *elements->now_DrpmB)>(*elements->DrpmB_up_speed))) {
        *elements->now_DrpmB = ((*elements->now_DrpmB)+((((double) *elements->DrpmB_up_speed) * (-1.0)) * (QEI_sampling_Sec)));
    } else {
        *elements->now_DrpmB = *elements->DrpmB;
    }
#endif

    /*PIDの係数計算*/

    /*  モータAのPID制御    */
    *(elements->now_rpmA1) = (double) ((((double) ((int16_t) (POS1CNT))) / ((QEI_sampling_Minute) * (QEI1_pulse_per_revolution)))*(-1.0));
    ErrorA3 = ErrorA2;
    ErrorA2 = ErrorA1;
    ErrorA1 = (((double) (*(elements->now_DrpmA)))-((double) (*(elements->now_rpmA1))));

    /*モータBのPID制御*/
#if 0
    rpm_B2 = *elements->now_rpmB1;
    *elements->now_rpmB1 = (int16_t) (((int16_t) (POS2CNT)) / ((QEI_sampling_Minute)*(QEI2_pulse_per_revolution)));

    DPB = ((*elements->P_B_gein) * ((double) (*elements->now_DrpmB - *elements->now_rpmB1)));
    DIB += ((*elements->I_B_gein) * ((double) (*elements->now_DrpmB - *elements->now_rpmB1)));
    DDB = ((*elements->D_B_gein) * (((double) (*elements->now_DrpmB - *elements->now_rpmB1)) - Last_errorB));
    Last_errorB = ((double) (*elements->now_DrpmB - *elements->now_rpmB1));
#endif
    /*dutyの算出*/
    last_dutyA = *(elements->now_dutyA);
    *(elements->now_dutyA) = (DPA_PID + DIA_PID + DDA_PID) + last_dutyA;

    if ((fabs(*(elements->now_dutyA))) >= (abs(PR2))) {
        if (*(elements->now_dutyA) > 0.0) {
            *(elements->now_dutyA) = ((float) (PR2));
        } else {
            *(elements->now_dutyA) = (((float) PR2) * (-1.0));
        }
    }

    setduty_OC(OC_for_MOTER_A, active_LOW, (*(elements->now_dutyA)));

#if 0
    *elements->now_dutyB = DPB + DIB + DDB;

    if ((fabs(*elements->now_dutyB)) >= ((double) PR2)) {
        if (*elements->now_dutyB > 0) {
            *elements->now_dutyB = ((double) (PR2));
        } else {
            *elements->now_dutyB = ((((double) PR2) * (-1.0)));
        }
    }
    setduty_OC(OC_for_MOTER_B, active_LOW, *elements->now_dutyB);
#endif

    /*LATの制御*/
    if ((*(elements->now_dutyA)) == 0) {
        SET_BRAKE_A(); //ブレーキ
    } else if ((*(elements->now_dutyA)) > 0) {
        SET_TRUN_NORMAL_A(); //正転
    } else {
        SET_TRUN_REVERSE_A(); //逆転
    }

#if 0
    if ((*(elements->now_dutyB) == 0) || (*elements->now_rpmB1 == 0)) {//考えるべきとこ
        SET_BRAKE_B(); //ブレーキ
    } else if (*elements->now_dutyB > 0) {
        SET_TRUN_NORMAL_B(); //正転
    } else {
        SET_TRUN_REVERSE_B(); //逆転
    }
#endif

    POS1CNT = 0;
    //POS2CNT = 0;
    return;

}







