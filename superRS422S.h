/* 
 * File:   superRS422S.h
 * Author: Shuuya
 *
 * Created on 2016/09/05, 23:50
 */

#ifndef SUPERRS422S_H
#define	SUPERRS422S_H

#ifdef	__cplusplus
extern "C" {
#endif


#include <xc.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include "dsPIC33FJ128_head.h"
#include "defines_for_RS422.h"
#include "RS422M.h"
#include "defines_for_API_RS422S.h"


#ifndef superRS422_TX_SW
#define superRS422_TX_SW LATBbits.LATB5
#endif

#ifndef superRS422_bps
#define superRS422_bps UART1_bps
#endif

#ifndef superRS422_TXREG
#define superRS422_TXREG U1TXREG
#endif

#ifndef superRS422_RXREG
#define superRS422_RXREG U1RXREG
#endif
    
#ifndef superRS422_URXDA
#define superRS422_URXDA U1STAbits.URXDA
#endif
    
#ifndef superRS422_TXIF  
#define superRS422_TXIF IFS0bits.U1TXIF
#endif

#ifndef superRS422_TXIE
#define superRS422_TXIE IEC0bits.U1TXIE
#endif

#ifndef superRS422_RXIF
#define superRS422_RXIF IFS0bits.U1RXIF
#endif  

#ifndef superRS422_ADDEN
#define superRS422_ADDEN U1STAbits.ADDEN
#endif  

#ifndef superRS422_TMR_ON_nOFF
#define superRS422_TMR_ON_nOFF T5CONbits.TON
#endif    

#ifndef superRS422_TMR
#define superRS422_TMR TMR5
#endif

#ifndef MAX_TMR_COUNT
#define MAX_TMR_COUNT 0xFFFF
#endif

#ifndef superRS422_TMR_TCKPS
#define superRS422_TMR_TCKPS T5CONbits.TCKPS
#endif      

#ifndef superRS422_TMRIE
#define superRS422_TMRIE IEC1bits.T5IE
#endif    

#ifndef superRS422_TMRIF
#define superRS422_TMRIF IFS1bits.T5IF
#endif

#ifndef MAX_DATA_SIZE
#define MAX_DATA_SIZE 32
#endif  

#ifndef MAX_DATA_KINDS
#define MAX_DATA_KINDS 32
#endif

#ifndef BUFFER_SIZE
#define BUFFER_SIZE (3+(MAX_DATA_SIZE))
#endif
    //deviceID,dataID,length,datas

    /*  マクロ関数  */
#define super_set_data_type(deviceID_num,dataID_num,data_type) device_id[ deviceID_num ].data_id[ dataID_num ].status.type= data_type ;
    //データ群のデータタイプを指定する関数,引数：(deviceID,dataID,指定するdataタイプ)
#define super_set_ACK_RS422()      {superRS422_TXIF   = 0;putUART_RS422(6   , UART_data_type);super_start_reply();}
#define super_set_NACK_RS422()     {superRS422_TXIF   = 0;putUART_RS422(0x15, UART_data_type);super_start_reply();}
#define super_putUART_RS422(datas,typeUART) {\
    superRS422_TXIF  = 0;\
    if (typeUART == UART_data_type) {\
        superRS422_TXREG = (((uint16_t)datas) & 0x00FF);\
    } else if (typeUART == UART_add_type) {\
        superRS422_TXREG = ((((uint16_t)datas) & 0x00FF) | 0x0100);\
    }\
    }    
    
#define super_start_transmit()      {super_state_RS422.now_RS422 = now_transmit;superRS422_TXIE   = 1;}//受信を許可
#define super_start_reply()         {super_state_RS422.now_RS422 = now_reply;superRS422_TXIE   = 1;}//受信を許可
#define super_start_recieve()       {super_state_RS422.now_RS422 = now_recieve;superRS422_TXIE =0;}//受信を遮断
#define super_start_idle()          {super_state_RS422.now_RS422 = now_idle;superRS422_TXIE =0;}
    
#define super_search_DataType device_id[super_search_deviceID].data_id[super_search_dataID].status.type 



    typedef union {
        uint8_t byte;

        struct {
            unsigned API_ESCdataIF : 1; //0x7Dを受け取ったか
            unsigned API_data_0x7E_IF : 1; //dataとして0x7Eを受け取った
            unsigned now_API_RS422 : 3; //現在の通信の状態
            unsigned now_RS422 : 3; //現在の通信状態
            unsigned OTHER : 1;
        };

    } data_API_RS422;
    
        typedef union {
        uint8_t byte;

        struct {
            unsigned now_RS422: 3;
            unsigned other: 5;
        };

    } data_state_RS422;
    
    extern uint8_t super_valid_data_RS422S[];
    extern uint8_t super_search_deviceID;
    extern uint8_t super_search_dataID;
    extern data_state_RS422 super_state_RS422;
    extern uint8_t super_now_buffer_position_S;
    extern uint8_t super_buffer_RS422S[(1 + MAX_DATA_SIZE)];
    
    void super_substitute_to_RS422(uint8_t*); 
    bool super_rx_ISR_RS422(uint8_t); 
    void super_tx_ISR_RS422(); 
    bool super_check_data_RS422S(uint8_t*, uint8_t); 
    void super_set_reply_mes(); 


#ifdef	__cplusplus
}
#endif

#endif	/* SUPERRS422S_H */

