#include <xc.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <p33FJ128MC802.h>  
#include "superRS422S.h"

uint8_t super_valid_data_RS422S[] = {};
uint8_t super_search_deviceID = 0;
uint8_t super_search_dataID = 0;
data_state_RS422 super_state_RS422 = {};
uint8_t super_now_buffer_position_S = 0;
uint8_t super_buffer_RS422S[(1 + MAX_DATA_SIZE)] = {};

/*コントローラ方式*/
#define superRS422_NowGetDevice direct_data[2]
#define superRS422_NowGetData   direct_data[4]
#define superRS422_NowGetLength direct_data[6]
#define superRS422_NowType     device_id[superRS422_NowGetDevice].data_id[superRS422_NowGetData].status.type
#define super_Max_RecieveData (superRS422_NowGetLength + 10)

void super_substitute_to_RS422(uint8_t* recieve_data) {

    uint8_t i;

    super_search_deviceID = recieve_data[0];
    super_search_dataID = recieve_data[1];
    device_id[super_search_deviceID].data_id[super_search_dataID].length = recieve_data[2];

    for (i = 0; i < device_id[recieve_data[0]].data_id[recieve_data[1]].length; i++) {
        device_id[recieve_data[0]].data_id[recieve_data[1]].data[i] = recieve_data[(i + 3)];
    }
    return;
}

bool super_rx_ISR_RS422(uint8_t using_UART) {

    static Controler_status status;
    static uint8_t direct_data[4 + 6 + (MAX_DATA_SIZE * 2)];
    uint8_t n, i;
    /*
    uint16_t* continue_read = &U1STA;
    if (using_UART == 1) {
        n = 0;
    } else {
        n = 1;
        for (i = 0; i < using_UART - 1; i++) {
            n = n * (i + 1);
        }
    }
    n = n * 16;
    
    continue_read = continue_read + n;*/

    if (super_state_RS422.now_RS422 == now_idle) {
        status.recievetime = 0;
    }
    super_start_recieve();
    while (superRS422_URXDA == 1) {
        direct_data[status.recievetime] = superRS422_RXREG;
        superRS422_RXIF = 0;
        
        if (super_check_data_RS422S(direct_data, status.recievetime) == true) {
            if (status.recievetime >= super_Max_RecieveData) {
                status.recievetime = 0;
                for (i = 0; i < (superRS422_NowGetLength + 3); i++) {
                    super_valid_data_RS422S[i] = direct_data[(i * 2) + 2];
                }
                super_substitute_to_RS422(direct_data); //RS422構造体に代入
                if (superRS422_NowType == superS_recieve_DATAtype) {
                    super_set_ACK_RS422();
                }//返信を開始
                else {
                    super_set_reply_mes();
                }
            } else {
                status.recievetime++;
            }
            status.useful = true;
        } else {
            status.useful = false;
            status.recievetime = 0;
        }
    }
    return status.useful;
}

void super_tx_ISR_RS422() {

    superRS422_TXIF = 0;
    if (super_state_RS422.now_RS422 == now_reply) {
        super_start_idle();
        return;
    }//送信データの誤送の可能性防止,0番目のデータは必ずこの関数以外で送信される,ACKの送信の場合 など

    if ((super_now_buffer_position_S == (device_id[super_search_deviceID].data_id[super_search_dataID].length + 1))
            && (device_id[super_search_deviceID].data_id[super_search_dataID].status.type == (superS_transmit_DATAtype))) {
        //送信データで、length分の送信が終わったとき
        super_now_buffer_position_S = 0;
        super_start_recieve() //受信の開始
        return;
    }
    super_start_transmit();
    super_putUART_RS422(super_buffer_RS422S[super_now_buffer_position_S], eight_bitsUARTtype); //次のデータを送信準備

    super_now_buffer_position_S++;
    return;

} //送信割り込み関数にて呼び出すべき関数（連続送信関数）

bool super_check_data_RS422S(uint8_t* data, uint8_t data_num) {
    //data 確認するデータ
    //data_num　現在データ番号

    /*  S   */
    if (data_num == 0) {
        return (data[data_num] == 'S');
    }/*　E   */
    else if (data_num == ((data[6] * 2) + 1)) {
        if (data[data_num] == 'E') {
            return true;
        } else {
            return false;
        }
    }        /*  2回目 SもしくはE*/
    else if ((data_num == 1) || (data_num == ((data[6]*2) + 2))) {
        return (data[data_num] == data[data_num - 1]); //２回目のSもしくはEのとき
    }/*  実データ部  */
    else if (data_num % 2 == 0) {
        return true; //偶数回目の受信
    } else {
        return (data[data_num] == (data[data_num - 1] ^ 0xFF));
    }//奇数回目で偶数回目の反転

    return false;
}

void super_set_reply_mes(void) {

    uint8_t i;
    super_buffer_RS422S[0] = super_search_deviceID;

    for (i = 0; i < device_id[super_search_deviceID].data_id[super_search_dataID].length; i++) {
        super_buffer_RS422S[i + 1] = device_id[super_search_deviceID].data_id[super_search_dataID].data[i];
    }
    super_start_reply();
    super_now_buffer_position_S = 1;
    super_putUART_RS422(super_buffer_RS422S[0], eight_bitsUARTtype);
    superRS422_TXIE = 1;

    return;
}

/*API(途中)*/
#if 0
rs422_data_position super_index_valid[(MAX_DEVICE_KINDS)*(MAX_DATA_KINDS)] = {};
data_API_RS422 state_API_RS422 = {};
uint8_t super_search_deviceID = 0;
uint8_t super_search_dataID = 0;

void super_rx_ISR_RS422(void) {

    static uint8_t recieve_data[2] = {};

    static uint8_t recieve_times = 0;
    uint8_t i = 0;

    /*0x7D受信時*/
    /*<<今のデータを見る>>*/
    if (state_API_RS422.API_ESCdataIF == OFF) {
        recieve_data[0] = superRS422_RXREG; //0x7DIFが必要
        if (recieve_data[0] == 0x7D) {
            state_API_RS422.API_ESCdataIF = ON;
            return;
        } else {
            state_API_RS422.API_ESCdataIF = OFF;
        }
    } else {
        recieve_data[1] = superRS422_RXREG;
        if (recieve_data[1] == 0x5D) {
            state_API_RS422.API_data_0x7E_IF = ON; //dataとして0x7Eを受け取った 
            state_API_RS422.API_ESCdataIF = OFF;
            recieve_data[0] = 0x7E;
            recieve_data[1] = 0;
        }
    }


    for (i = 0; i < (state_API_RS422.API_ESCdataIF + 1); i++) {//0x7DがESCコードでないとき2周する
        /* <<API_StartDataの受信>> */
        if ((state_API_RS422.API_data_0x7E_IF == OFF) && (state_API_RS422.now_API_RS422 == API_state_idle)) {
            if (recieve_data[i] == API_StartData) {
                state_API_RS422.now_API_RS422 = API_state_start;
            } else {
                state_API_RS422.now_API_RS422 = API_state_idle;
            }
        }/*<<device_id>>*/
        else if (state_API_RS422.now_API_RS422 == API_state_start) {//前の状態がスタートバイト受信
            super_search_deviceID = recieve_data[i];
            state_API_RS422.now_API_RS422 = API_state_DeviceId;
        }/*<<data_id>>*/
        else if (state_API_RS422.now_API_RS422 == API_state_DeviceId) {//前の状態がdevice_id受信
            super_search_dataID = recieve_data[i];
            state_API_RS422.now_API_RS422 = API_state_DataId;
        }/*<<length>>*/
        else if (state_API_RS422.now_API_RS422 == API_state_DataId) {//前の状態がdata_id受信
            device_id[super_search_deviceID].data_id[super_search_dataID].length = recieve_data[i];
            state_API_RS422.now_API_RS422 = API_state_length;
        }/*<<実data>>*/
        else if (state_API_RS422.now_API_RS422 == API_state_length) {//前の状態がlength受信
            device_id[super_search_deviceID].data_id[super_search_dataID].data[recieve_times] = recieve_data[i];
            recieve_times++;
            if (recieve_times == device_id[super_search_deviceID].data_id[super_search_dataID].length) {//length分の受信
                state_API_RS422.now_API_RS422 = API_state_data;
            }
        }/*<<チェックサム>>*/
        else if (state_API_RS422.now_API_RS422 == API_state_data) {//前の状態がdata受信
            state_API_RS422.now_API_RS422 = API_state_CheckSum;
            recieve_times = 0;
            if (super_search_DataType == superS_recieve_DATAtype) {//受信データ型なら
                super_set_ACK_RS422(); //返信mesのset

                return;
            }//返信する
        }//受信データ時
    }
}

void super_tx_ISR_RS422(void) {

    superRS422_TXIF = 0;

    if (state_API_RS422.now_API_RS422 == API_state_reply) {//<<今>>の状態が返信中
        if (super_search_DataType == superS_recieve_DATAtype) {
            super_start_idle(); //待機状態へ移行
            return;
        } else if (super_search_DataType == superS_transmit_DATAtype) {
            //length分まで
            //次のデータをsetする。
        }
    }



}
#endif










