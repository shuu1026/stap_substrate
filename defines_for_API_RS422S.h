/* 
 * File:   defines_for_API_RS422S.h
 * Author: Shuuya
 *
 * Created on 2016/09/06, 9:35
 */

#ifndef DEFINES_FOR_API_RS422S_H
#define	DEFINES_FOR_API_RS422S_H

#ifdef	__cplusplus
extern "C" {
#endif
    
    /*RS422構造体のデータ型*/
    

    /*通信状態、段階*/
#define API_state_idle (0) //待ち状態中
#define API_state_start (1) //スタートバイト受信状態中
#define API_state_DeviceId (2) //device_idの受信中
#define API_state_DataId (3) //data_idの受信中
#define API_state_length (4) //lengthの受信中
#define API_state_data (5) //実dataの受信中
#define API_state_CheckSum (6) //チェックサムの受信中
#define API_state_reply (7) //応答中
#define API_state_transmit (7) //返信中
#define API_state_REreply (8) //返信の受信中
    
    



#ifdef	__cplusplus
}
#endif

#endif	/* DEFINES_FOR_API_RS422S_H */

